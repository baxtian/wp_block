# DESCRIPTION

Class to be inherited to create a WP block.

Use *register_block_type* o register a new block.

Use *extend_block* to extend an existing block.

Declare a *render* method inside the class to have a *render_callback* for the created / extended block.

## Mantainers

Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>

## Changelog

### 0.3.25

* Allow setting blocks to be available on particular post types.

### 0.3.24

* Allow blocks to be inserted in site-editor.php

### 0.3.23

* [Bug] Prefix does not wor correctly if there isn't a block.js.

### 0.3.22

* Include block for dashboard scripts.

### 0.3.20

* Set global variables on declaration.

### 0.3.19

* Using baxtian/wp_columns.

### 0.3.17

* Allow to set post types for plugin block.

### 0.3.11

* Allow to add a block that creates a sidebar plugin.

### 0.3.10

* Set minimum php verion to 7.4

### 0.3.9

* Include if in widgets.

### 0.3.6

* Include if in new post.

### 0.3.5

* If in administrator, include only if in editor.

### 0.3.4

* Use render for extended block only outside the admin.

### 0.3.3

* Solve error with repo.

### 0.3.2

* Use static values from Merak.

### 0.3.1

* Use assets.php only with scripts.

### 0.3

* In case of using this class in multiple providers, allow Composer to set which file to use by default.

### 0.2.16

* Include initialize in contructor.

### 0.2.15

* Add function to optimize block creation with merak.

### 0.2.11

* Duplicated separator in prefix.

### 0.2.10

* Use directory as prefix for filenames.

### 0.2.9

* Use prefix for filenames.

### 0.2.8

* Allow to use PHP8.0

### 0.2.7

* In case of empty list of global variables.

### 0.2.6

* Solve parser warnings.

### 0.2.4

* Do not enqueue bootstrap if we are in admin.

### 0.2.3

* Set render function correctly in case we are extending the a block. 

### 0.2.2

* Enqueue styles and scripts in case of a extended block

### 0.2.1

* Fixing bugs

### 0.2

* Unify script and style for blocks

### 0.1.6

* Fix declaration errors

### 0.1.1

* Include className into attributes 

### 0.1

* First stable release
