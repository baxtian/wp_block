<?php

namespace Baxtian;

use Baxtian\WP_Columns_Postmeta;
use WP_Block_Type_Registry;
use WP_Block_Editor_Context;

/**
 * Clase padre de bloques
 */
class WP_Block
{
	// Variables
	protected $prefix;
	protected $domain;
	protected $blockname;
	protected $classname;
	protected $attributes;
	protected $dependencies;
	protected $dirs;
	protected $merak;
	protected $post_types = [];

	private $global_attributes;

	protected $column_position = false;
	protected $handle;
	private $wp_columns = false;

	protected function __construct()
	{
		if ($this->merak) {
			$this->initialize($this->merak);
		}

		if($this->column_position !== false) {
			$this->wp_columns = new WP_Columns_Postmeta();
		}

		$this->handle = str_replace('_', '-', strtolower($this->classname));

		// Declarar uso de estas dependencias de bloques si tenemos dependencias
		// para ser usadas al momento de la declaración del bloque
		if (is_array($this->dependencies) && count($this->dependencies) > 0) {
			add_filter('global_block_dependencies', function ($dependencies) {
				if (!is_array($dependencies) || count($dependencies) == 0) {
					$dependencies = [
						'block'     => [],
						'editor'    => [],
						'frontend'  => [],
						'style'     => [],
						'dashboard' => [],
					];
				}

				// Recorrer atributos y mezclarlos
				foreach ($dependencies as $key => $items) {
					if (isset($this->dependencies[$key])) {
						$dependencies[$key] = array_unique(array_merge($items, $this->dependencies[$key]));
					}
				}

				return $dependencies;
			});
		}

		// Buscar las variable asignadas como globales
		$this->global_attributes = [];
		if (is_array($this->attributes) && count($this->attributes) > 0) {
			foreach ($this->attributes as $key => $item) {
				if (isset($item['global']) && $item['global']) {
					$this->global_attributes[$key] = $item;
				}
			}
		}

		// Crear filtro para usar las variables globales
		if (count($this->global_attributes) > 0) {
			add_filter('global_block_attributes', function ($attributes) {
				if (!is_array($attributes)) {
					$attributes = [];
				}

				return array_merge($attributes, $this->global_attributes);
			});
		}

		// Habilitar este bloque solo para los tipos definidiso
		add_filter('allowed_block_types_all', [$this, 'allowed_blocks'], 10, 2);
	}

	/**
	 * Generar los datos que deben ser calculados, declarar la funcionalidad para el
	 * Rest API, declarar el uso de edición en bloque y edición rápida, y declarar las
	 * columnas para cada estructura con la que se vincule este PostMeta.
	 */
	protected function init()
	{

	}

	/**
	 * Inicializar las variables usando la instancia de Merak
	 *
	 * @param Object $merak
	 * @return void
	 */
	protected function initialize($merak = false)
	{
		if ($merak) {
			$this->prefix = $merak::PREFIX;
			$this->domain = $merak::DOMAIN;
			$this->dirs   = [
				'url'       => $merak->url_path() . '/build/',
				'path'      => $merak->dir_path() . 'build' . DIRECTORY_SEPARATOR,
				'lang_path' => $merak->languages_dir(),
			];
		}
	}

	/**
	 * Inicializar las columnas
	 *
	 * @return void
	 */
	protected function setColumns()
	{
		if($this->wp_columns !== false) {
			$this->wp_columns->init(
				$this->post_types,
				$this->handle,
				$this->attributes,
				$this->column_position
			);
		}
	}

	/**
	 * Crea la generación del bloque usando los scripts y estilos compilados en el
	 * directorio build/[$this->classname]
	 * Buscará los siguientes archivos:
	 * 		block.js - script del bloque
	 * 		editor.css - estilo exclusivo del backend
	 * 		frontend.js - scripts del frontend
	 * 		style.css - estilos de frontend y backend
	 */
	protected function register_block_type(): void
	{
		// Asignar columnas
		$this->setColumns();

		// Si aun no están registrados los datos o no está activo Gutenberg no continuar
		if (empty($this->blockname) || empty($this->classname) || ! function_exists('register_block_type')) {
			return;
		}

		// Registrar scripts y estilos generales
		$atts = $this->register_assets();

		// ¿Se están declarando atributos?
		$attributes = apply_filters('global_block_attributes', $this->attributes);
		if (!empty($attributes)) {
			$atts['attributes'] = $attributes;
		}

		// ¿Existe una función de render?
		if (method_exists($this, 'render')) {
			$atts['render_callback'] = [$this, '_render'];
		}

		register_block_type(
			$this->blockname,
			$atts
		);
	}

	/**
	 * Incluir los scripts y estilos compilados en el directorio build sin necesidad
	 * de declarar un bloque. Este método está diseñado para extender capacidades de
	 * bloques ya existentes
	 *
	 * @return void
	 */
	protected function extend_block(): void
	{
		// Asignar columnas
		$this->setColumns();

		// Si aun no están registrados los datos o no está activo Gutenberg no continuar
		if (empty($this->classname) || ! function_exists('register_block_type')) {
			return;
		}

		// Registrar scripts y estilos generales
		$atts = $this->register_assets();

		// Si estamos en entorno de administración
		if (is_admin()) {
			// Si hay script de bloque y aun no está en cola
			if (!empty($atts['editor_script']) && !wp_script_is($atts['editor_script'])) {
				wp_enqueue_script($atts['editor_script']);
			}

			// Si hay estilo de editor de bloque y aun no está en cola
			if (!empty($atts['editor_style']) && !wp_style_is($atts['editor_style'])) {
				wp_enqueue_style($atts['editor_style']);
			}

			// Si hay script de dashboard y aun no está en cola
			if (!empty($atts['dashboard_script']) && !wp_script_is($atts['dashboard_script'])) {
				wp_enqueue_script($atts['dashboard_script']);
			}
		} else { // Estamos en frontend
			// Si hay script de frontend y aun no está en cola
			if (!empty($atts['script']) && !wp_script_is($atts['script'])) {
				wp_enqueue_script($atts['script']);
			}
		}

		// Si hay estilo de frontend y aun no está en cola
		if (!empty($atts['style']) && !wp_style_is($atts['style'])) {
			wp_enqueue_style($atts['style']);
		}

		// ¿Existe una función de render y tenemos nombre del bloque?
		if (method_exists($this, 'render') && !empty($this->blockname)) {
			// Asignar la función de render
			$atts['render_callback'] = [$this, '_render'];

			// Buscar entre los bloques declarados si ya está este bloque
			$blocks = WP_Block_Type_Registry::get_instance()->get_all_registered();
			$block  = (isset($blocks[$this->blockname])) ? $blocks[$this->blockname] : false;

			// Si existe el bloque, modificar el render
			if ($block) {
				// Solo si no estamos en el administrador usaremos
				// nuestro propio render. En el administrador usaremos el
				// render por defecto del bloque.
				// ¿Deseas tu propio render? Entonces has un bloque nuevo.
				if (!is_admin()) {
					$block->set_props($atts);
				}
			} else { // Si no existe el bloque crearlo
				register_block_type(
					$this->blockname,
					$atts
				);
			}
		}
	}

	/**
	 * Incluir los scripts y estilos compilados en el directorio build sin necesidad
	 * de declarar un bloque. Este método está diseñado para declarar los atributos
	 * para ser usados por un plugin de sidebar.
	 *
	 * @return void
	 */
	protected function plugin_block(): void
	{
		$this->extend_block();

		// Declaración de las variables para ser consultadas por los bloques
		// de plugin en las estructuras reportadas
		foreach($this->post_types as $post_type) {
			// Agregar capacidad de campos custom
			// si la estrtuctura no lo tiene
			add_post_type_support($post_type, 'custom-fields');
			foreach($this->attributes as $key => $attribute) {
				register_post_meta($post_type, $key, [
					'show_in_rest' => true,
					'single'       => true,
					'type'         => $attribute['type'],
				]);
			}
		}

		add_action('current_screen', [$this, 'inline_block_script']);
	}

	/**
	 * Agregar los datos que usará el script del bloque d eplugin
	 *
	 * @return void
	 */
	public function inline_block_script()
	{
		// Inicializar la variable con los tipos de estructuras donde
		// se cargará este bloque de plugin
		$var = sprintf(
			"var %s_block_var_%s = { post_types: ['%s'] }",
			$this->prefix,
			sanitize_title($this->classname),
			implode("','", $this->post_types)
		);
		wp_add_inline_script($this->prefix . '_blocks', $var);
	}

	/**
	 * Determinar el prefijo que se usará para los archivos
	 *
	 * @param string $nameNOmbre a buscar
	 * @return string El prefijo
	 */
	private function get_prefix($name, $dirs, $prefix = '')
	{
		// Si ya tenemos prefijo, usarlo
		if ($prefix != '') {
			return $prefix;
		}

		// Revisar si existe con el prefijo 'gtm_' o con 'gtmbrg/'
		if (file_exists($dirs['path'] . 'gtm_' . $name . '.asset.php')) {
			$prefix = 'gtm_';
		} elseif (file_exists($dirs['path'] . DIRECTORY_SEPARATOR . 'gtmbrg' . DIRECTORY_SEPARATOR)) {
			$prefix = 'gtmbrg' . DIRECTORY_SEPARATOR;
		}

		return $prefix;
	}

	/**
	 * Registrar los scripts y estilos compilados para el bloque
	 */
	private function register_assets(): array
	{
		// Si estamos en el administrador solo necesitamos cargar
		// el gutenberg si estamos editando o activando los widgets
		global $pagenow;
		$allow_admin_gutemberg = (
			is_admin() &&
			in_array(
				$pagenow,
				['post.php', 'post-new.php', 'widgets.php', 'site-editor.php']
			)
		) ? true : false;

		$allow_front_gutemberg = (
			!is_admin()
		) ? true : false;

		$allow_admin_dashboard = (
			is_admin()
		) ? true : false;

		// Directorios
		$dirs = shortcode_atts(
			[
				'url'       => [],
				'path'      => [],
				'lang_path' => [],
			],
			$this->dirs
		);

		// Inicializar arreglod e atributos
		$atts = [];

		// Dependencias globales
		$dependencies = apply_filters('global_block_dependencies', [
			'block'     => [],
			'editor'    => [],
			'frontend'  => [],
			'style'     => [],
			'dashboard' => [],
		]);

		$prefix = false;

		// Version
		$version = get_bloginfo('version');

		// Bloque y estilo de editor se activan solo si está
		// habilitado para ello
		if($allow_admin_gutemberg) {
			// Script del bloque, traducciones y localización
			// BLOCK
			$prefix = $this->get_prefix('block', $dirs);
			if (file_exists($dirs['path'] . $prefix . 'block.asset.php')) {
				$atts['editor_script'] = $this->prefix . '_blocks';

				// Solo es necesario registrar si aun no está registrado
				if (!wp_script_is($this->prefix . '_blocks', 'registered')) {
					$asset_file = include($dirs['path'] . $prefix . 'block.asset.php');

					// Version
					$version = $asset_file['version'];

					// Dependencias
					$_dependencies = array_unique(array_merge($asset_file['dependencies'], $dependencies['block']));

					wp_register_script($this->prefix . '_blocks', $dirs['url'] . $prefix . 'block.js', $_dependencies, $asset_file['version'], true);
					wp_set_script_translations($this->prefix . '_blocks', $this->domain, $dirs['lang_path']);
					wp_localize_script($this->prefix . '_blocks', $this->prefix . '_block_var', ['prefix' => $this->prefix, 'domain' => $this->domain]);
				}
			}

			// Estilo solo en backend
			// EDITOR
			$prefix = $this->get_prefix('editor', $dirs, $prefix);

			if (file_exists($dirs['path'] . $prefix . 'editor.css')) {
				$atts['editor_style'] = $this->prefix . '_block-editor';

				// Solo es necesario registrar si aun no está registrado
				if (!wp_style_is($this->prefix . '_block-editor', 'registered')) {
					// Dependencias
					$_dependencies = array_unique(array_merge([], $dependencies['editor']));

					wp_register_style($this->prefix . '_block-editor', $dirs['url'] . $prefix . 'editor.css', $_dependencies, $version);
				}
			}
		}

		// Frontend del bloque se activan solo si está
		// habilitado para ello
		if($allow_front_gutemberg) {
			// Script en el frontend
			// FRONTEND
			$prefix = $this->get_prefix('frontend', $dirs, $prefix);

			if (file_exists($dirs['path'] . $prefix . 'frontend.asset.php')) {
				$atts['script'] = $this->prefix . '_block-frontend';

				// Solo es necesario registrar si aun no está registrado
				if (!wp_script_is($this->prefix . '_block-frontend', 'registered')) {
					$asset_file = include($dirs['path'] . $prefix . 'frontend.asset.php');

					// Version
					$version = $asset_file['version'];

					// Dependencias
					$_dependencies = array_unique(array_merge($asset_file['dependencies'], $dependencies['frontend']));

					wp_register_script($this->prefix . '_block-frontend', $dirs['url'] . $prefix . 'frontend.js', $_dependencies, $asset_file['version'], true);
				}
			}
		}

		// Estilo de editor se activan solo si está
		// habilitado para administrador o para front
		if($allow_admin_gutemberg || $allow_front_gutemberg) {
			// Estilo en frontend y backend
			// STYLE
			$prefix = $this->get_prefix('style', $dirs, $prefix);

			if (file_exists($dirs['path'] . $prefix . 'style.css')) {
				$atts['style'] = $this->prefix . '_block-style';

				// Solo es necesario registrar si aun no está registrado
				if (!wp_style_is($this->prefix . '_block-style', 'registered')) {
					// Dependencias
					$_dependencies = array_unique(array_merge([], $dependencies['style']));

					// ¿Está bootstrap?
					$btst_pos = array_search('bootstrap', $_dependencies);
					if ($btst_pos !== false) {
						// Si es así y estamos en admin, retirarla
						if (is_admin()) {
							unset($_dependencies[$btst_pos]);
						}
					}

					wp_register_style($this->prefix . '_block-style', $dirs['url'] . $prefix . 'style.css', $_dependencies, $version);
				}
			}
		}

		// Script de dashboard se activan solo si está
		// habilitado para ello
		if($allow_admin_dashboard) {
			// Script de dashboard
			// DASHBOARD
			$prefix = $this->get_prefix('dashboard', $dirs);
			if (file_exists($dirs['path'] . $prefix . 'dashboard.asset.php')) {
				$atts['dashboard_script'] = $this->prefix . '_dashboard';

				// Solo es necesario registrar si aun no está registrado
				if (!wp_script_is($this->prefix . '_dashboard', 'registered')) {
					$asset_file = include($dirs['path'] . $prefix . 'dashboard.asset.php');

					// Version
					$version = $asset_file['version'];

					// Dependencias
					$_dependencies = array_unique(array_merge($asset_file['dependencies'], $dependencies['dashboard']));

					wp_register_script($this->prefix . '_dashboard', $dirs['url'] . $prefix . 'dashboard.js', $_dependencies, $asset_file['version'], true);
					wp_set_script_translations($this->prefix . '_dashboard', $this->domain, $dirs['lang_path']);
					wp_localize_script($this->prefix . '_dashboard', $this->prefix . '_dashboard_var', ['prefix' => $this->prefix, 'domain' => $this->domain]);
				}
			}
		}

		return $atts;
	}

	/**
	 * Obtener la etiqueta de apertura y cierre del bloque para ser usada en
	 * las plantillas y asi no perder las funcionalidades extra
	 * @param  array  $attributes Conjunto de atributos definidos en el bloque.
	 * @param  string $content    Contenido creado inicialmente por el bloque.
	 * @return string             Contenido a ser publicado.
	 */
	protected function attributes($attributes, $content): array
	{
		// Eliminar espacios blancos entre etiquetas
		$content = preg_replace('/(\>)\s*(\<)/m', '$1$2', trim($content));

		// Inicializar valores
		$attributes['block'] = [
			'open'    => '',
			'close'   => '',
			'content' => '',
		];

		// Parcear documento
		if (!empty($content)) {
			$doc = new \DOMDocument();
			libxml_use_internal_errors(true);
			@$doc->loadHTML($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
			$_content = $doc->saveHTML();

			// Usar el primer hijo, que suponemps es la raiz
			$parent = $doc->childNodes->item(0);

			// Si hay elementos a extraer
			if ($parent) {
				// Mientras tenga hijos, eliminarlos
				while (count($parent->childNodes) > 0 && $parent->childNodes->item(0) !== null) {
					// Eliminar el primer hijo
					$parent->removeChild($parent->childNodes->item(0));
				}

				// Explotarlos por la unión de apertura y cierre de la etiqueta
				$ans = explode('></', $doc->saveHTML());

				// Guardar la apertura y el cierre
				$attributes['block'] = [
					'open'    => $ans[0] . '>',
					'close'   => '</' . $ans[1],
					'content' => str_replace([$ans[0] . '>', '</' . $ans[1]], '', $_content),
				];
			}
		}

		//Si ya hay variable de contenido, usar otra variable.
		//En caso contrario incluirla
		if (isset($attributes['content'])) {
			$attributes['extra_content'] = $content;
		} else {
			$attributes['content'] = $content;
		}

		// Extender classname
		$classname = [];
		// Si en los atributos vienen un classname, extraer todos los elementos y crear
		// un arreglo con las clases
		if (isset($attributes['className'])) {
			$classname = explode(' ', $attributes['className']);
		}
		// Agregar el wp-block-{blockname} reemplazando / por -
		$classname[] = 'wp-block-' . str_replace('/', '-', $this->blockname);
		// Eliminar cualquier elemento repetido
		array_unique($classname);

		// Crea rnuevo classname
		$attributes['className'] = implode(' ', $classname);

		return $attributes;
	}

	/**
	 * Filtro para habilitar bloques solo en estructuras concretas
	 *
	 * @param array $allowed_block_types
	 * @param WP_Block_Editor_Context $block_editor_context
	 * @return array
	 */
	public function allowed_blocks($allowed_block_types, $block_editor_context)
	{
		// Si estamos en el editor de patrones o no hay estructuras específicas
		// retornar la lista de tipos de bloques permitidos completa.
		if($block_editor_context->name == 'core/edit-site' || $this->post_types == []) {
			return $allowed_block_types;
		}

		// Si no está la estructura del post actual en la lista de
		// estructuras permitidas retirar el actual bloque de la lista
		if(!in_array($block_editor_context->post->post_type, $this->post_types)) {
			$all_blocks = [];
			if(is_array($allowed_block_types)) {
				// Si llega un arreglo usarlo
				$all_blocks = $allowed_block_types;
			} elseif($allowed_block_types) {
				// Si es true hay que crear la lista con todos los bloques registrados
				$registered_blocks = WP_Block_Type_Registry::get_instance()->get_all_registered();
				foreach ($registered_blocks as $registered_block) {
					$all_blocks[] = $registered_block->name;
				}
			} else {
				// Si es false retornar false
				return false;
			}

			// Bloques a retirar
			$disallowed_blocks = [
				$this->blockname,
			];

			// Retornar solo los bloques permitidos.
			$allowed_block_types = array_values(array_diff($all_blocks, $disallowed_blocks));
		}

		// Retornar la lista de bloques permitidos
		return $allowed_block_types;
	}

	/**
	 * Organizar los elementos previos para posteriormente llamar al renderizado
	 * @param  array  $attributes Conjunto de atributos definidos en el bloque.
	 * @param  string $content    Contenido creado inicialmente por el bloque.
	 * @return string             Contenido a ser publicado.
	 */
	public function _render($attributes, $content): string
	{
		$attributes = $this->attributes($attributes, $content);

		return call_user_func([$this, 'render'], $attributes, $content);
	}
}
